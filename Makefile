.SILENT:
.PHONY: help wait
.DEFAULT_GOAL = help

APP_ENV=prod
include .env
-include .env.local

RESET			= \033[0m
TEXT_BOLD		= \033[1m
TEXT_DIM		= \033[2m
TEXT_ITALIC		= \033[3m
TEXT_UNDERLINE	= \033[4m
TEXT_BLINK		= \033[5m
COLOR_ERROR		= \033[31m
COLOR_INFO		= \033[32m
COLOR_COMMENT	= \033[33m

PROJECT		= OrchestreMassenet
EXEC_PHP	= php
CONSOLE		= $(EXEC_PHP) bin/console
PHPUNIT		= $(EXEC_PHP) bin/phpunit
PHPCSFIXER	= ./vendor/bin/php-cs-fixer
PHPINSIGHTS	= ./vendor/bin/phpinsights
PHPSTAN		= ./vendor/bin/phpstan
PHPMETRICS	= ./vendor/bin/phpmetrics
SYMFONY		= symfony
COMPOSER	= composer
YARN		= yarn
DOCKER		= docker compose

## Show all targets available
help:
	printf "${COLOR_COMMENT}Usage:${RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-20s${RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST) | sort

wait:
	sleep 5



### ————— Composer 🧙‍♂️ —————
.PHONY: install install-dev install-prod

## Composer - Create composer.lock from composer.json
composer.lock: composer.json
	$(COMPOSER) update

## Composer - Install vendors according to the current composer.lock file
install: composer.lock dev
	$(COMPOSER) install --prefer-dist

## Composer/Yarn - Install vendors according to the current composer.lock file for production
install-prod: composer.lock
	$(COMPOSER) install --optimize-autoloader --classmap-authoritative --apcu-autoloader --no-dev
	$(YARN) install

## Composer/Yarn - Install vendors according to the current composer.lock file for development
install-dev: composer.lock
	$(COMPOSER) install
	$(YARN) install



### ————— Symfony 🎵 —————
.PHONY: cc warmup fix-perms assets purge reset-dev reset-dev-with-docker

cc:
	$(CONSOLE) c:c

warmup:
	$(CONSOLE) cache:warmup

fix-perms:
	chmod -R 777 var/*

## Assets - Install the assets with symlinks in the public folder
assets:
	$(CONSOLE) assets:install public/ --symlink --relative

## Cache/Logs - Purge cache and logs
purge:
	rm -rf var/cache/* var/logs/*

## Reset dev environnement and reinstall it keeping docker's related files (var/docker)
reset-dev:
ifeq "$(APP_ENV)" "dev"
	printf "Removing var/cache, vendor, public/build, public/media, public/uploads, sitemaps and node_modules\n"
	rm -rf var/cache vendor public/build public/media public/uploads public/sitemap.* node_modules
	printf "Reinstalling project\n\n"
	$(COMPOSER) install && $(YARN) install && $(YARN) dev
else
	printf "This command is only available in ${COLOR_ERROR}dev${RESET}, you are currently using ${COLOR_ERROR}${APP_ENV}${RESET}\n"
endif

## Reset dev environnement and reinstall it
reset-dev-with-docker: docker-down
ifeq "$(APP_ENV)" "dev"
	printf "Removing var/cache, var/docker, vendor, public/build, public/media, public/uploads, sitemaps and node_modules\n"
	rm -rf var/cache var/docker vendor public/build public/media public/uploads public/sitemap.* node_modules
	printf "Reinstalling project\n\n"
	$(COMPOSER) install && $(YARN) install && $(YARN) dev
else
	printf "This command is only available in ${COLOR_ERROR}dev${RESET}, you are currently using ${COLOR_ERROR}${APP_ENV}${RESET}\n"
endif



### ————— Symfony binary 💻 —————
.PHONY: serve unserve

## Symfony - Serve the application with HTTPS support
serve:
	$(SYMFONY) serve --port=8000

## Symfony - Stop the webserver
unserve:
	$(SYMFONY) server:stop

tail:
	symfony server:log

open:
	symfony open:local


### ————— Yarn 🐱 / JavaScript —————
.PHONY: watch build-dev build

## Yarn - Watch files and build assets when needed for the dev env
watch:
	$(YARN) run encore dev --watch

build-dev:
	$(YARN) run encore dev

## Yarn - Build assets for production
build:
	$(YARN) run encore production --progress



### ————— Symfony Messenger —————
.PHONY: stop-workers

## Messenger - Stop all workers
stop-workers:
	$(CONSOLE) messenger:stop-workers



### ————— Doctrine —————
.PHONY: dsv

## Doctrine - Validate the mapping files
dsv:
	$(CONSOLE) doctrine:schema:validate



### ————— LiipImagine —————
.PHONY: liip-cache-remove

## Liip - Remove all cache
liip-cache-remove:
	$(CONSOLE) liip:imagine:cache:remove



### ————— Installations —————
.PHONY: prod dev dump-env-prod

dump-env-prod:
	$(COMPOSER) dump-env prod

## Install the project in production mode
prod: install-prod stop-workers cc assets dump-env-prod build dsv
	printf "\n${COLOR_INFO}→ All done 👍🏻 ${RESET}\n\n"

## Install the project in development mode
dev: install-dev stop-workers cc assets build-dev analyse dsv
	printf "\n${COLOR_INFO}→ All done : Dev environment is ready to be used 👍🏻 ${RESET}\n\n"



### ————— Tests —————
.PHONY: insight phpstan phpmetrics tests analyse db-tests-load

## PHPInsights - Start analyse of the project
insights: vendor
	$(PHPINSIGHTS)

## PHPStan - Start analyse of the project, using phpstan.neon
phpstan: vendor phpstan.dist.neon
	$(PHPSTAN) analyse

## PhpMetrics - Start analyse of the project
phpmetrics: vendor .phpmetrics.json
	$(PHPMETRICS) --config=.phpmetrics.json

db-tests-load: vendor
db-tests-load: export APP_ENV=test
db-tests-load:
	$(CONSOLE) doctrine:database:drop --if-exists --force || true
	$(CONSOLE) doctrine:database:create
	$(CONSOLE) doctrine:migrations:migrate -n
	$(CONSOLE) doctrine:fixtures:load -n

db-tests-drop: vendor
db-tests-drop: export APP_ENV=test
db-tests-drop:
	$(CONSOLE) doctrine:database:drop --force || true

## Start tests (PHPUnit)
tests: db-tests-load
tests: export APP_ENV=test
tests:
	$(PHPUNIT) $@
	$(MAKE) db-tests-drop

## Analyse the project (composer & doctrine)
analyse:
	printf "➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}Symfony : check security${RESET}\n"
	$(SYMFONY) check:security
	printf "\n➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}Composer : valid${RESET}\n"
	$(COMPOSER) valid
	printf "\n➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}Lint : Yaml${RESET}\n"
	$(CONSOLE) lint:yaml config --parse-tags
	printf "\n➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}Lint : Twig (prod)${RESET}\n"
	$(CONSOLE) lint:twig templates --env=prod
	printf "\n➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}Lint : Container${RESET}\n"
	$(CONSOLE) lint:container --no-debug
	printf "\n➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}Doctrine : Schema validate${RESET}\n"
	$(CONSOLE) doctrine:schema:validate
	printf "\n➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}PHPStan : analyse${RESET}\n"
	$(PHPSTAN) analyse
	printf "\n➡️  ${TEXT_UNDERLINE}${TEXT_BOLD}PHPCSFixer : fix (dry-run) with diff and verbose mode${RESET}\n"
	$(PHPCSFIXER) fix --diff --verbose --dry-run



### ————— Docker 🐳 —————
.PHONY: docker-create-directories docker-launch-containers docker-start docker-stop docker-rm docker-down logs

docker-create-directories:
	mkdir -p ./var/docker/mariadb
	mkdir -p ./var/docker/exchange
	mkdir -p ./var/docker/rabbitmq/log

docker-launch-containers:
	$(SYMFONY) server:stop
	$(DOCKER) up -d --wait
	#$(SYMFONY) server:start -d
	#$(SYMFONY) console messenger:setup-transports
	#$(SYMFONY) run -d --watch=config,src,templates,vendor
#messenger:consume async
	#$(SYMFONY) run -d npx encore dev-server

## Docker - Create the needed directories and start containers
docker-start: docker-create-directories docker-launch-containers

## Docker - Stop containers
docker-stop:
	$(DOCKER) stop
	$(SYMFONY) server:stop

# Docker - Stop containers and delete linked volumes
docker-down:
	$(DOCKER) down -v --remove-orphans
	$(SYMFONY) server:stop

## Docker - Delete containers and their linked volumes
docker-rm:
	$(DOCKER) rm -v

## Docker - Delete local directories
docker-delete-local: docker-down
	rm -rf ./var/docker

## Docker - Display logs
docker-logs:
	$(DOCKER) logs --tail=0 --follow
